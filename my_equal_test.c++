#include <iostream> // cout, endl
#include <sstream>  // istringtstream, ostringstream
#include <string>   // string
#include <utility>  // pair
#include <algorithm> // equal
#include <functional> // function
#include <list> //list
#include <vector> //vector

#include "gtest/gtest.h"
#include "my_equal.h"

using namespace std;
using testing::TestWithParam;
using testing::Values;

using Equal_List_Signature = function<bool (list<int>::const_iterator, list<int>::const_iterator, vector<int>::const_iterator)>;

struct Equal_List_Fixture : TestWithParam<Equal_List_Signature> 
	{};

INSTANTIATE_TEST_CASE_P(
    Equal_List_Instantiation,
    Equal_List_Fixture,
    Values(
           equal<list<int>::const_iterator, vector<int>::const_iterator>,
        my_equal<list<int>::const_iterator, vector<int>::const_iterator>));

TEST_P(Equal_List_Fixture, test_1) {
	list<int> l{2, 3, 4};
	vector<int> v{2, 3, 4};

	ASSERT_TRUE(GetParam()(l.begin(), l.end(), v.begin()));
}

TEST_P(Equal_List_Fixture, test_2) {
	list<int> l{2, 3, 4};
	vector<int> v{2, 3, 4, 5, 6};

	ASSERT_TRUE(GetParam()(l.begin(), l.end(), v.begin()));
}

TEST_P(Equal_List_Fixture, test_3) {
	list<int> l{2, 3, 4, 5, 6};
	vector<int> v{2, 3, 4};

	ASSERT_FALSE(GetParam()(l.begin(), l.end(), v.begin()));
}

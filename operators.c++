#include <iostream>
#include <cassert> //assert

using namespace std;

void minus() {
	cout << "Minus" << endl;
	int x = 10;
	int y = 38;
	int z = y - x;

	x -= 38;
	assert(x == -28);
	assert(z == 28);
}

void multiplication() {
	cout << "Multiplication" << endl;
	int x = 10;
	int y = 23;
	int z = x * y;

	x *= y;
	assert(x == 230);
	assert(z == 230);
}

void division() {
	cout << "Division" << endl;
	int x = 10;
	int y = 2000;
	int z = y/x;

	y /= x;
	assert(z == 200);
	assert(y == 200);
	cout << "Division Done" << endl;
}

void bitwise_and() {
	cout << "Bitwise And" << endl;
	int x = 72;
	int y = 184;
	int z = x & y;

	x &= y;
	assert(x == 8);
	assert(z == 8);
	cout << "Bitwise And Done" << endl;
}

void bitwise_or() {
	cout << "Bitwise Or" << endl;
	int x = 72;
	int y = 184;
	int z = x | y;

	x |= y;
	assert (z == 248);
	assert (x == 248);
	cout << "Bitwise Or done." << endl;
}

void bitwise_xor() {
	cout << "Bitwise XOR" << endl;
	int x = 114;
	int y = 170;
	int z = x ^ y;

	x ^= y;
	assert(z == 216);
	assert(x == 216);
	cout << "Bitwise XOR done." << endl;
}

void bitshift() {
	cout << "Bitshift" << endl;
	int x = 114;
	int y = (x >> 1);

	x >>= 1;
	assert (y == 57);
	assert (x == 57);

	y = 170;
	int z = (y << 1);

	y <<= 1;
	assert (z == 340);
	assert (y == 340);
	cout << "Bitshift done." << endl;
}
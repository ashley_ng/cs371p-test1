#include <cstddef>  // ptrdiff_t
#include <iterator> // input_iterator_tag

using namespace std;

template<typename T>

class Range {
private:
	T start;
	T finish;
public:
	//constructor
	Range(T b, T e) {
		start = b;
		finish = e;
	}

	class iterator {
	public:
	    using iterator_category = input_iterator_tag;
	    using value_type        = T;
	    using difference_type   = ptrdiff_t;
	    using pointer           = T*;
	    using reference         = T&;
	    
	private:
		T value;
	public:
		//constructor
		iterator(T v) {
			value = v;
		}

		//equal
		bool operator==(const iterator i) const {
			return value == i.value;
		}
		//not equal
		bool operator!=(const iterator i) const {
			return value != i.value;
		}
		// *
		T operator*() const {
			return value;
		}
		//++ pre and post
		// pre
		iterator& operator++() {
			++value;
			return *this;
		}
		//post
		iterator operator++(int) {
			iterator copy(value);
			++value;
			return copy;
		}
	};

	//begin
	iterator begin() {
		return iterator(start);
	}
	//end
	iterator end() {
		return iterator(finish);
	}
};
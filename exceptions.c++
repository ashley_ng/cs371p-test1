#include <cassert> //assert
#include <cstring> //strcmp
#include <iostream> //cout, endl
#include <stdexcept> // domain_error
#include <string> // string

using namespace std;

int throw_exception1(bool b) {
	if (b) {
		throw domain_error("abc");
	}
	return 0;
}

int throw_exception2(bool b) {
	if (b) {
		throw 15;
	}
	return 0;
}


void throw_exception_example1() {
	cout << "thow exception 1" << endl;

	try{
		int ret_val = throw_exception1(false);
		assert (ret_val == 0);
	}
	catch (domain_error &e) {
		assert (false); // never gets here
	}
	cout << "thow exception 1 done." << endl;
}

void throw_exception_example2() {
	cout << "thow exception 2" << endl;
	try {
		int ret_val = throw_exception1(true);
		assert (false); // never gets here
	}
	catch (domain_error &e) {
		assert(string(e.what()) == "abc");
	}
	cout << "thow exception 2 done." << endl;
}

/*
	Doesn't go to second catch, will go up the call chain
	Will terminate with uncaught exception of type char, 
	because no call currently catches it
*/
int throw_exception_example3() {
	cout << "thow exception 3" << endl;
	try {
		int ret_val = throw_exception2(true);
		assert (false); // never gets here
	}
	catch(int e) {
		throw 'e';
	}
	catch(char e) {
		assert(false); // never gets here
	}
	cout << "thow exception 3 done." << endl;
}
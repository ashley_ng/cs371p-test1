#include <algorithm> // equal
#include <cassert>   // assert
#include <iostream>  // cout, endl
#include <list>      // list

#include "gtest/gtest.h"

#include "my_range.h"

using namespace std;

TEST(Range_Fixture, class_test_1) {
    Range<int> x(2, 2);
    Range<int>::iterator b = x.begin();
    Range<int>::iterator e = x.end();
    ASSERT_EQ(b, e);}

TEST(Range_Fixture, class_test_2) {
    Range<int> x(2, 3);
    Range<int>::iterator b = x.begin();
    Range<int>::iterator e = x.end();
    ASSERT_NE(b, e);
    ASSERT_EQ(2, *b);
    ++b;
    ASSERT_EQ(b, e);}

TEST(Range_Fixture, class_test_3) {
    Range<int> x(2, 4);
    Range<int>::iterator b = x.begin();
    Range<int>::iterator e = x.end();
    ASSERT_NE(b, e);
    ASSERT_EQ(2, *b);
    ++b;
    ASSERT_NE(b, e);
    ASSERT_EQ(3, *b);
    b++;
    ASSERT_EQ(b, e);}

TEST(Range_Fixture, class_test_4) {
    Range<int> x(2, 5);
    list<int>  y = {2, 3, 4};
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin()));}
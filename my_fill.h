
//weakest iterator would be Forward, but could use Input
template<typename InputIterator, typename T>
void my_fill(InputIterator begin, InputIterator end, T value) {
	while (begin != end) {
		*begin = value;
		++begin;
	}
}
#include <cstddef>  // ptrdiff_t
#include <iterator> // input_iterator_tag

using namespace std;

template<typename T>

class Range_Iterator {
// must have this, but out of scope of class
public:
    using iterator_category = input_iterator_tag;
    using value_type        = T;
    using difference_type   = ptrdiff_t;
    using pointer           = T*;
    using reference         = T&;

private:
	T value;

	/*
		Things I need:
			constructor
			Equal
			not equal
			++(pre and post)
			*
	*/
public:
	//constructor 
	Range_Iterator(T v) {
		value = v;
	}

	//equal
	// const method, doesn't modify calling object
	bool operator==(const Range_Iterator r) const {
		return (value == r.value);
	}

	//not equal
	bool operator!=(const Range_Iterator r) const {
		return (value != r.value);
	}

	//++ pre incrememnt. 
	//Need to return an l-value so user can change value if they want
	Range_Iterator& operator++() {
		++value;
		return *this; //this is a pointer to me, need to return a reference to me
	}

	// ++ post increment
	// user can't alter what is returned
	Range_Iterator operator++(int) {
		Range_Iterator copy = *this;
		++value;
		return copy;
	}

	// dereference operator
	T operator*() const {
		return value;
	}
};
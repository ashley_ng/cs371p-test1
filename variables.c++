#include <cassert> // assert
#include <iostream> // cout, endl

#include function_headers.h

void copy() {
	cout << "Copy" << endl;
	int x = 2;
	int y = i; // just a copy of i
	++y; 
	assert(x == 2);
	assert y == 3);
	assert(&x != &y); // not the same address
	cout << "Copy done." << endl;
}

void pointer() {
	cout << "Pointer" << endl;
	int x = 2;
	int *y = &x; // y is a reference to x
	++*y; // need *, otherwise incrementing pointer address
	assert (x == 3);
	assert (*y == 3);
	assert (y == &x); //y has the same address as x
	/*
		j:2 <- p
		Legal not initially set pointer
		int *p;
		p = &j;
	*/
	cout << "Pointer done." << endl;
}

void reference() {
	cout << "Reference" << endl;
	int x = 2;
	int &y = x;
	++y;
	assert(x = 3);
	assert (y == 3);
	assert(&x == &y);
	/*
		x, y: 2
		Reference has to be assigned, following isn't legal
		int &r;
		r = &k;
	*/
	cout << "Reference Done." << endl;
}

void pointer_reference() {
	cout << "Pointer Reference" << endl;
	int x = 2;
	int *y = &i;
	int *&z = y;
	++*z; // have to have star to dereference pointer, otherwise just altering pointer

	assert(x == 3);
	assert(*y == 3);
	assert(*z == 3); // z point to x
	assert(&z == &y); // z and y point to the same reference
	cout << "Pointer Reference Done" << endl;
}
#include <cassert> //assert
#include <iostream> // cout endl

#include "function_headers.h"

using namespace std;

void pass_by_value(int x) {
	++x; // modifing local copy of x
}

void pass_by_address(int *x) {
	++*x; //modifies value x is pointing too
}

void pass_by_reference(int &x) {
	++x; //alters the value x references
}

void value() {
	cout << "Value" << endl;
	int x = 2;
	pass_by_value(x);
	assert(x == 2); // function doesn't alter x in this scope
	cout << "Value done." << endl;
}

void address() {
	cout << "Address" << endl;
	int x = 2;
	pass_by_address(&x);
	assert(x == 3); // function alter's x in this scope
	cout << "Address done" << endl;
}

void reference() {
	cout << "Reference" << endl;
	int x = 2;
	pass_by_reference(x);
	assert(x == 3); // function alters x in this scope
	cout << "Reference done" << endl;
}
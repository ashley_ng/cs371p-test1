#include <vector> //vector
#include <cstddef>  // ptrdiff_t
#include <iterator> // input_iterator_tag

using namespace std;

template<typename T>

class MyStack {
private:
	vector<T> container;

public:

	//push to top of stack
	void push(T v) {
		container.insert(container.begin(), v);
	}

	//pop front of stack
	void pop() {
		container.erase(container.begin());
	}

	//peek
	T peek() {
		return container[0];
	}

	//size
	int size() {
		return container.size();
	}

	class MyIterator {
	 	public:
        	using iterator_category = input_iterator_tag;
            using value_type        = T;
            using difference_type   = ptrdiff_t;
            using pointer           = T*;
            using reference         = T&;

        private:
        	int index;
        	T value;
        	MyStack *mystack;
        public: 
        	//constructor
        	MyIterator(int i, MyStack *r) {
        		index = i;
        		mystack = r;
        		value = r->container[index];
        	}
            // MyIterator(MyIterator &it) {
            //     index = it.index;
            //     mystack = it.mystack;
            //     value = it.value;
            // }
        	//equal
        	bool operator==(const MyIterator &it) const{
        		return value != it.value;
        	}
        	//not equal
        	bool operator!=(const MyIterator &it) const{
        		return value != it.value;
        	}
        	//dereference
        	T operator*() const {
        		return value;
        	}
        	//++ pre and post 
        	// pre
        	MyIterator& operator++() {
        		value = mystack->container[++index];
        		return *this;
        	}
        	//post
        	MyIterator operator++(int) {
        		MyIterator copy(value);
        		value = mystack->container[++index];
        		return copy;
        	}
        	//-- pre and post;
        	//pre
        	MyIterator& operator--() {
        		value = mystack->container[--index];
        		return *this;
        	}
        	//post
        	MyIterator operator--(int) {
        		MyIterator copy(value);
        		value = mystack->container[--index];
        		return copy;
        	}
	};

	MyIterator begin() {
		return MyIterator(0, this);
	}
	MyIterator end() {
		return MyIterator(container.size(), this);
	}
};
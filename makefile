CXX        := clang++
CXXFLAGS   := -pedantic -std=c++11 -Wall
LDFLAGS    := -lgtest -lgtest_main -pthread

Other: main.c++ function_headers.h exceptions.c++ operators.c++ arguments.c++
	$(CXX) $(CXXFLAGS) main.c++ exceptions.c++ operators.c++ arguments.c++ -o RunOther
	./RunOther

Queue: my_queue.h my_queue_test.c++
	$(CXX) $(CXXFLAGS) my_queue_test.c++ -o RunMyQueue $(LDFLAGS)
	./RunMyQueue

Stack: my_stack.h my_stack_test.c++
	$(CXX) $(CXXFLAGS) my_stack_test.c++ -o RunMyStack $(LDFLAGS)
	./RunMyStack

Range: my_range.h my_range_test.c++
	$(CXX) $(CXXFLAGS) my_range_test.c++ -o RunMyRange $(LDFLAGS)
	./RunMyRange

RangeIterator: my_range_iterator.h my_range_iterator_test.c++
	$(CXX) $(CXXFLAGS) my_range_iterator_test.c++ -o RunMyRangeIterator $(LDFLAGS)
	./RunMyRangeIterator

EqualTest: my_equal.h my_equal_test.c++
	$(CXX) $(CXXFLAGS) my_equal_test.c++ -o RunMyEqual $(LDFLAGS)
	./RunMyEqual

CopyTest: my_copy.h my_copy_test.c++
	$(CXX) $(CXXFLAGS) my_copy_test.c++ -o RunMyCopy $(LDFLAGS)
	./RunMyCopy

FillTest: my_fill.h my_fill_test.c++
	$(CXX) $(CXXFLAGS) my_fill_test.c++ -o RunMyFill $(LDFLAGS)
	./RunMyFill

AllOfTest: my_all_of.h my_all_of_test.c++
	$(CXX) $(CXXFLAGS) my_all_of_test.c++ -o RunMyAllOf $(LDFLAGS)
	./RunMyAllOf

CountTest: my_count.h my_count_test.c++
	$(CXX) $(CXXFLAGS) my_count_test.c++ -o RunMyCount $(LDFLAGS)
	./RunMyCount

AccumulateTest: my_accumulate.h my_accumulate_test.c++
	$(CXX) $(CXXFLAGS) my_accumulate_test.c++ -o RunMyAccumulate $(LDFLAGS)
	./RunMyAccumulate

FindTest: my_find.h my_find_test.c++
	$(CXX) $(CXXFLAGS) my_find_test.c++ -o RunMyFind $(LDFLAGS)
	./RunMyFind

RemoveTest: my_remove.h my_remove_test.c++
	$(CXX) $(CXXFLAGS) my_remove_test.c++ -o RunMyRemove $(LDFLAGS)
	./RunMyRemove

ReverseTest: my_reverse.h my_reverse_test.c++
	$(CXX) $(CXXFLAGS) my_reverse_test.c++ -o RunMyReverse $(LDFLAGS)
	./RunMyReverse

SwapTest: my_swap.h my_swap_test.c++
	$(CXX) $(CXXFLAGS) my_swap_test.c++ -o RunMySwap $(LDFLAGS)
	./RunMySwap
	
TransformTest: my_transform.h my_transform_test.c++
	$(CXX) $(CXXFLAGS) my_transform_test.c++ -o RunMyTransform $(LDFLAGS)
	./RunMyTransform

clean:
	-rm RunOther
	-rm RunMyStack
	-rm RunMyRange
	-rm RunMyRangeIterator
	-rm RunMyEqual
	-rm RunMyCopy
	-rm RunMyFill
	-rm RunMyAllOf
	-rm RunMyCount
	-rm RunMyAccumulate
	-rm RunMyFind
	-rm RunMyRemove
	-rm RunMyReverse
	-rm RunMySwap
	-rm RunMyTransform
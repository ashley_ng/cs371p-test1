
// Exception examples
void throw_exception_example1();
void throw_exception_example2();
void throw_exception_example3();

//Operators
void division();
void bitwise_and();
void bitwise_or();
void bitwise_xor();
void bitshift();

//variables
void copy();
void pointer();
void reference();
void pointer_reference();

//Pass by arguments
void value();
void address();
void reference();

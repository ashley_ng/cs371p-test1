template<typename InputIterator, typename T>

int my_count(InputIterator begin, InputIterator end, T value) {
	int count = 0;
	while (begin != end) {
		if (*begin == value)
			++count;
		++begin;
	}
	return count;
}
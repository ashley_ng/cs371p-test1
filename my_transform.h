template<typename InputIterator, typename OutputIterator, typename Func>
OutputIterator my_transform(InputIterator begin, InputIterator end, OutputIterator container, Func f) {
	while (begin != end) {
		*container = f(*begin);
		++begin;
		++container;
	}
	return container;
}
#include <functional> // function
#include <iostream>   // cout, endl
#include <vector>       // vector
#include <algorithm> // transform, equal
#include <list> //list

#include "gtest/gtest.h"
#include "my_transform.h"

using namespace std;
using testing::TestWithParam;
using testing::Values;

using Transform_Vector_Signature = function<vector<int>::iterator (list<int>::const_iterator, list<int>::const_iterator, vector<int>::iterator, function<int (int)>)>;
struct Tranform_Vector_Fixture : TestWithParam<Transform_Vector_Signature>
	{};
INSTANTIATE_TEST_CASE_P(
	Transform_Vector_Instatiation,
	Tranform_Vector_Fixture,
	Values(
		transform<list<int>::const_iterator, vector<int>::iterator, function<int (int)>>,
		my_transform<list<int>::const_iterator, vector<int>::iterator, function<int (int)>>
	)
);

TEST_P(Tranform_Vector_Fixture, test_1) {
	list<int> l{2, 3, 4, 5};
	const vector<int> plus_five{7, 8, 9, 10};
	vector<int> v(4);
	vector<int>::iterator it = GetParam()(l.begin(),  l.end(), v.begin(), [ ](int v)->int {return v+5;});
	ASSERT_TRUE(equal(v.begin(), v.end(), plus_five.begin()));
}

TEST_P(Tranform_Vector_Fixture, test_2) {
	list<int> l{2, 3, 4, 5};
	const int value = 3;
	const vector<int> mod_three{2, 0, 1, 2};
	vector<int> v(4);
	vector<int>::iterator it = GetParam()(l.begin(),  l.end(), v.begin(), [value](int v)->int {return v%value;});
	ASSERT_TRUE(equal(v.begin(), v.end(), mod_three.begin()));
}
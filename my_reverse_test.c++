#include <functional> // function
#include <iostream>   // cout, endl
#include <list>       // list
#include <algorithm> // reverse, equal

#include "gtest/gtest.h"
#include "my_reverse.h"

using namespace std;
using testing::TestWithParam;
using testing::Values;

using Reverse_List_Signature = function<void (list<int>::iterator, list<int>::iterator)>;
struct Reverse_List_Fixture : TestWithParam<Reverse_List_Signature>
	{};
INSTANTIATE_TEST_CASE_P	(
	Reverse_List_Instantiation,
	Reverse_List_Fixture,
	Values(
		reverse<list<int>::iterator>,
		my_reverse<list<int>::iterator>
	)
);

TEST_P(Reverse_List_Fixture, test_1) {
	list<int> l{2, 3, 4, 5};
	const list<int> reversed{5, 4, 3, 2};
	GetParam()(l.begin(), l.end());
	ASSERT_EQ(*l.begin(), *reversed.begin());
	for (int value : l) {
		cout << value << endl;
	}
	ASSERT_TRUE(equal(l.begin(), l.end(), reversed.begin()));
}
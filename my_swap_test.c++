#include <functional> // function
#include <iostream>   // cout, endl
#include <vector>       // vector
#include <algorithm> // swap

#include "gtest/gtest.h"
#include "my_swap.h"

using namespace std;
using testing::TestWithParam;
using testing::Values;

using Swap_Vector_Signature = function<void (vector<int>, vector<int>)>;
struct Swap_Vector_Fixture : TestWithParam<Swap_Vector_Signature>
	{};
INSTANTIATE_TEST_CASE_P(
	Swap_Vector_Instantiation,
	Swap_Vector_Fixture,
	Values(
		swap<vector<int>>,
		my_swap<vector<int>>
	)
);

TEST_P(Swap_Vector_Fixture, test_1) {
	vector<int> first{2, 3, 4};
	vector<int> second{5, 6, 7};
	const vector<int> original_first{2, 3, 4};
	const vector<int> original_second{5, 6, 7};
	GetParam()(first, second);
// 	ASSERT_EQ(first, original_second);
// 	ASSERT_EQ(second, original_first);
}
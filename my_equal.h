
template<typename InputIterator1, typename InputIterator2>
bool my_equal (InputIterator1 begin, InputIterator1 end, InputIterator2 container) {
    while (begin != end) {
        if (*begin != *container)
            return false;
        ++begin;
        ++container;
    }
    return true;
}
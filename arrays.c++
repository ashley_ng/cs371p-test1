#include <cassert> //assert
#include <iostream> //cout, endl;

#include "function_headers.h"

using namespace std;

int array1() {
	cout << "Array 1" << endl;
	int a[] = {2, 3, 4};
	assert(*a == a[0]); //value at 'a' is the first argument in array
	assert(a == &a[0]); //'a' is a pointer to the first first argument in array
	
}
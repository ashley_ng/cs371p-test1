
template<typename InputIterator, typename OutputIterator>

OutputIterator my_copy(InputIterator begin, InputIterator end, OutputIterator containter) {
	while (begin != end) {
		*containter = *begin;
		++containter;
		++begin;
	}
	return containter;
}
#include <cassert> //assert
#include <iostream> //cout, endl

#include "function_headers.h"

using namespace std;

// variable const_int is read-only
void const1() {
	cout << "const 1" << endl;
	// const int const_int; // illegal, can't leave a const unitialized
	const int const_int = 5;
	// ++const_int; // illegal, can't incrememnt a const
	assert(const_int == 5);
	cout << "const 3 done" << endl;
}

void const2() {
	cout << "const 2" << endl;
	int x = 3;
	const int const_int = 4;
	int *p;
	p = &x;
	++*p;
	assert(x == 4);
	// p = &const_int; // can convert p(read/write pointer) to read only int
	++x;
	assert(x == 5);
	assert(*p == 5);
	cout << "const 2 done." << endl;
}

void const3() {
	cout << "const 3" << endl;
	int x = 3;
	const int const_int = 4;
	const int *const_pointer; //why is this legal? why can you leave this unitialized?
	const_pointer = &const_int;
	// ++*const_pointer; //can't alter read only 
	// int *pointer = const_pointer; // can't put a read only pointer in a read/write pointer
	int* pointer = const_cast<int*>(const_pointer); //cast const_pointer to regular pointer
	// ++*pointer; // it still points to 
}
#include <algorithm>  // equal, transform
#include <cassert>    // assert
#include <functional> // function
#include <iostream>   // cout, endl
#include <list>       // list
#include <numeric>    // accumulate
#include <vector>     // vector

using namespace std;

using binary_func1 = int (*) (int, int); // oridinary C function
using binary_func2 = function<int (int, int)>; // abstraction

int add_function(int i, int j) {
	return i + j;
}

/*
	regular function to other
*/
void regular_func() {
	const list<int> l{2, 3, 4};

	binary_func1 x = add_function;
	binary_func2 y = add_function;
	auto z = add_function; // detects the return type

	// show that all do the same thing
	assert(add_function(2, 3) == 5);
	assert(x(2, 3) == 5);
	assert(y(2, 3) == 5);
	assert(z(2, 3) == 5);

	// show all do the same thing with accumulate()
	assert(accumulate(l.begin(), l.end(), 0, add_function) == 9);
	assert(accumulate(l.begin(), l,end(), 0, x) == 9);
	assert(accumulate(l.begin(), l.end(), 0, y) == 9);
	assert(accumulate(l.begin(), l.end(), 0, z) == 9);
} 

/*
	lambdas to other
*/
void lambdas_func() {
	const list<int> l{2, 3, 4};

	binary_func1 x = [] (int x, int y) -> int {return i + j;};
	binary_func2 y = [] (int x, int y) -> int {return i + j;};
	auto z = [] (int x, int y) -> int {return i + j;};

	// show that all do the same thing
	assert([] (int x, int y) -> int {return i + j;}(2, 3) == 5);
	assert(x(2, 3) == 5);
	assert(y(2, 3) == 5);
	assert(z(2, 3) == 5);

	// show all do the same thing with accumulate()
	assert(accumulate(l.begin(), l.end(), 0, add_function) == 9);
	assert(accumulate(l.begin(), l,end(), 0, x) == 9);
	assert(accumulate(l.begin(), l.end(), 0, y) == 9);
	assert(accumulate(l.begin(), l.end(), 0, z) == 9);
}
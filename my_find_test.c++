#include <functional> // function
#include <iostream>   // cout, endl
#include <list>       // list

#include "gtest/gtest.h"
#include "my_find.h"

using namespace std;
using testing::TestWithParam;
using testing::Values;

using Find_Vector_Signature = function<vector<int>::const_iterator (vector<int>::const_iterator, vector<int>::const_iterator, int)>;
struct Find_Vector_Fixture : TestWithParam<Find_Vector_Signature>
	{};
INSTANTIATE_TEST_CASE_P(
	Find_Vector_Instantiation,
	Find_Vector_Fixture,
	Values(
		find<vector<int>::const_iterator, int>,
		my_find<vector<int>::const_iterator, int>
	)
);

TEST_P(Find_Vector_Fixture, test_1) {
	vector<int> v{2, 3, 4};
	const int find = 4;
	vector<int>::const_iterator found = GetParam()(v.begin(), v.end(), find);
	ASSERT_EQ(found, v.begin() + 2);
}

TEST_P(Find_Vector_Fixture, test_2) {
	vector<int> v{2, 3, 4};
	const int find = 5;
	vector<int>::const_iterator found = GetParam()(v.begin(), v.end(), find);
	ASSERT_EQ(found, v.end());
}

TEST_P(Find_Vector_Fixture, test_3) {
	vector<int> v{2, 3, 4};
	const int find = 2;
	vector<int>::const_iterator found = GetParam()(v.begin(), v.end(), find);
	ASSERT_EQ(found, v.begin());
}


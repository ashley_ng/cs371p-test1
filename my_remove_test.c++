#include <functional> // function
#include <iostream>   // cout, endl
#include <vector>       // vector
#include <algorithm> // remove

#include "gtest/gtest.h"
#include "my_remove.h"

using namespace std;
using testing::TestWithParam;
using testing::Values;

using Remove_Vector_Signature = function<vector<int>::iterator (vector<int>::iterator, vector<int>::iterator, int value)>;
struct Remove_Vector_Fixture : TestWithParam<Remove_Vector_Signature>
	{};
INSTANTIATE_TEST_CASE_P(
	Remove_Vector_Instantiation,
	Remove_Vector_Fixture,
	Values(
		remove<vector<int>::iterator, int>,
		my_remove<vector<int>::iterator, int>
	)
);

TEST_P(Remove_Vector_Fixture, test_1) {
	vector<int> v{4, 5, 6, 7};
	const int value = 5;
	const vector<int> after{4, 6, 7};
	vector<int>::iterator removed = GetParam()(v.begin(), v.end(), value);
	ASSERT_EQ(*(--removed), *(after.begin()+2));
	ASSERT_EQ(*(--removed), *(after.begin()+1));
	ASSERT_EQ(*(--removed), *(after.begin())); 
}

TEST_P(Remove_Vector_Fixture, test_2) {
	vector<int> v{4, 5, 6, 5, 7, 9, 5};
	const int value = 5;
	const vector<int> after{4, 6, 7, 9};
	vector<int>::iterator removed = GetParam()(v.begin(), v.end(), value);
	ASSERT_EQ(*(--removed), *(after.begin()+3));
	ASSERT_EQ(*(--removed), *(after.begin()+2));
	ASSERT_EQ(*(--removed), *(after.begin()+1));
	ASSERT_EQ(*(--removed), *(after.begin())); 
}
template<typename BidirectionalIterator>
void my_reverse(BidirectionalIterator begin, BidirectionalIterator end) {
	--end;
	BidirectionalIterator temp = begin;
	while(begin != end) {
		*temp = *begin;
		*begin = *end;
		*end = *temp;
		++begin;
		--end;
	}
}
template<typename ForwardIterator, typename T, typename Func>
T my_accumulate(ForwardIterator begin, ForwardIterator end, T seed, Func f) {
	while (begin != end) {
		seed = f(seed, *begin);
		++begin;
	}
	return seed;
}

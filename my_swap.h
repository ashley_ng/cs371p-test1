template<typename T>
void my_swap(T &first, T &second) {
	T temp = second;
	second = first;
	first = temp;
}
# Test 1 Concepts

### C++ Types:
- [X] bool
- [X] char
- [X] double
- [X] float
- [ ] int istream
- [ ] istream_iterator
- [X] istringstream
- [X] list
- [X] long
- [ ] ostream
- [ ] ostream_iterator
- [X] ostringstream
- [X] queue
- [ ] set 
- [X] short
- [X] stack
- [X] vector

### C++ Operators
- [X] - and -=
- [ ] !
- [ ] []
- [X] * and *=
- [X] / and /=
- [ ] & and && (logical ops)
- [X] &= (bitwise and)
- [ ] % and %=
- [X] ^ and ^= (bitwise xor)
- [ ] + and +=
- [X] << and <<= (bitshift)
- [ ] = >>
- [X] >>= (bitshift)
- [ ] | and || (logical ops)
- [X] |= (bitwise or)

### C++ Algorithms
- [X] [accumulate](http://www.cplusplus.com/reference/numeric/accumulate/)
	- [ ] accumulate(first, last, seed)
	- [] accumulate(first, last, seed, func)
- [X] [all_of](http://www.cplusplus.com/reference/algorithm/all_of/)
	- [X] all_of(first, last, func)
- [X] [copy](http://www.cplusplus.com/reference/algorithm/copy/)
	- [X] copy(first, last, container)
- [X] [count](http://www.cplusplus.com/reference/algorithm/count/)
	- [X] count(first, last, val)
- [X] [equal](http://www.cplusplus.com/reference/algorithm/equal/)
	- [X] equal(first1, last1, first2)
	- [ ] equal(first1, last1, first2, func)
- [X] [fill](http://www.cplusplus.com/reference/algorithm/fill/)
	- [X] fill(first, last, val)
- [X] [find](http://www.cplusplus.com/reference/algorithm/find/)
	- [X] fill(first, last, val)
- [X] [remove](http://www.cplusplus.com/refrence/algorithm/remove/)
	- [X] remove(first, last, val)
- [ ] [reverse](http://www.cplusplus.com/reference/algorithm/reverse/)
	- [ ] reverse(first, last)
- [ ] [swap](http://www.cplusplus.com/reference/algorithm/swap/)
	- [ ] swap(obj1, obj2)
- [X] [transform](http://www.cplusplus.com/reference/algorithm/transform/)
	- [ ] transform(first, last, container, func)
	- [X] transform(first, last, container, first, func)

### C++ Concepts
- [ ] #include
- [ ] assertions
- [X] bidrectional iterator
- [ ] capture by reference
- [ ] capture by value
- [ ] catch
- [ ] cin 
- [ ] class templates
- [X] closures
- [X] const methods
- [X] const pointers
- [ ] const references
- [X] const values
- [X] cout
- [X] exceptions
- [X] forward iterator
- [X] function templates
- [X] input iterator
- [ ] iteration
- [ ] iterators
- [ ] l-values
- [X] lambdas
- [ ] nested classes
- [ ] opeators
- [X] output iterators
- [ ] recursion
- [ ] return by address
- [ ] return by reference
- [ ] return by value
- [ ] structs
- [ ] throw
- [ ] try
- [ ] using

### C++ Tokens
- [ ] "*" on a type
- [ ] "*" on a variable
- [ ] "&" on a type
- [ ] "&" on a variable

### Concepts From Class
- [ ] standard diviation
- [ ] z-score

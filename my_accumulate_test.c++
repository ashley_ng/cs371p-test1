#include <functional> // function
#include <iostream>   // cout, endl
#include <list>       // list
#include <numeric> // accumulate

#include "gtest/gtest.h"
#include "my_accumulate.h"

using namespace std;
using testing::TestWithParam;
using testing::Values;

using Accumulate_List_Signature = function<int (list<int>::const_iterator, list<int>::const_iterator, int, function<int (int, int)>)>;
struct Accumulate_List_Fixture : TestWithParam<Accumulate_List_Signature>
	{};
INSTANTIATE_TEST_CASE_P(
	Accumulate_List_Instantiation,
	Accumulate_List_Fixture,
	Values(
		accumulate<list<int>::const_iterator, int, function<int (int, int)>>,
		my_accumulate<list<int>::const_iterator, int, function<int (int, int)>>
	)
);

TEST_P(Accumulate_List_Fixture, test_1) {
	list<int> l{2, 3, 4};
	int seed = 5;
	ASSERT_EQ(14, GetParam()(l.begin(), l.end(), seed, [ ](int seed, int v)->int {return seed + v;}));
}

TEST_P(Accumulate_List_Fixture, test_2) {
	list<int> l{2, 3, 4};
	int seed = 1;
	ASSERT_EQ(24, GetParam()(l.begin(), l.end(), seed, [ ](int seed, int v)->int {return seed * v;}));
}

TEST_P(Accumulate_List_Fixture, test_3) {
	list<int> l{2, 3, 4};
	int seed = 10;
	ASSERT_EQ(1, GetParam()(l.begin(), l.end(), seed, [ ](int seed, int v)->int {return seed - v;}));
}

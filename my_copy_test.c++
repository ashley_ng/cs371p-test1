#include <algorithm>  // copy, equal
#include <cassert>    // assert
#include <functional> // function
#include <iostream>   // cout, endl
#include <list>       // list
#include <vector>     // vector

#include "gtest/gtest.h"

#include "my_copy.h"

using namespace std;

using testing::TestWithParam;
using testing::Values;

using Copy_List_Signature = function<vector<int>::iterator (list<int>::const_iterator, list<int>::const_iterator, vector<int>::iterator)>;

struct Copy_List_Fixture : TestWithParam<Copy_List_Signature>
    {};

INSTANTIATE_TEST_CASE_P(
    Copy_List_Instantiation,
    Copy_List_Fixture,
    Values(
           copy<list<int>::const_iterator, vector<int>::iterator>,
        my_copy<list<int>::const_iterator, vector<int>::iterator>));

TEST_P(Copy_List_Fixture, class_example) {
    const list<int>       x = {2, 3, 4};
    vector<int>           y(5);
    vector<int>::iterator p = GetParam()(x.begin(), x.end(), y.begin() + 1);
    ASSERT_EQ(p, y.begin() + 4);
    ASSERT_TRUE(equal(x.begin(), x.end(), y.begin() + 1));}


TEST_P(Copy_List_Fixture, test_1) {
	const list<int> l{2, 3, 4};
	vector<int> v(5);
	vector<int>::iterator copy = GetParam()(l.begin(), l.end(), v.begin());
	ASSERT_TRUE(equal(l.begin(), l.end(), v.begin()));
	ASSERT_EQ(copy, v.begin() + 3);
}
/*
	removes all instances of value
*/
template<typename InputIterator, typename T>
InputIterator my_remove(InputIterator begin, InputIterator end, T value) {
	InputIterator ret_val = begin;
	while (begin != end) {
		if (*begin != value) {
			*ret_val = *begin;
			++ret_val;
		}
		++begin;
		
	}
	return ret_val;
}
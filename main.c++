#include <iostream> // cout, endl
#include "function_headers.h"

using namespace std;

int main() {
	// throw_exception_example1();
	// throw_exception_example2();
	// throw_exception_example3();

	// division();
	// bitwise_and();
	// bitwise_or();
	// bitwise_xor();
	// bitshift();

	// copy();
	// pointer();
	// reference();
	// pointer_reference();

	// value();
	// address();
	// reference();

	const int x = 7;
	int y = const_cast<int>(x);
	
	return 0;
}
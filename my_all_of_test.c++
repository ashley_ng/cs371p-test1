#include <algorithm>  // all_of
#include <cassert>    // assert
#include <functional> // function
#include <iostream>   // cout, endl
#include <list>       // list

#include "gtest/gtest.h"

#include "my_all_of.h"

using namespace std;

using testing::TestWithParam;
using testing::Values;

using AllOf_List_Signature = function<bool (list<int>::const_iterator, list<int>::const_iterator, function<bool (int)>)>;
struct AllOf_List_Fixture : TestWithParam<AllOf_List_Signature>
	{};

INSTANTIATE_TEST_CASE_P (
	AllOf_List_Instantiation,
	AllOf_List_Fixture,
	Values (
		all_of<list<int>::const_iterator, function<bool (int)>>,
		my_all_of<list<int>::const_iterator, function<bool (int)>>
	)
);

TEST_P(AllOf_List_Fixture, class_example_1) {
    const list<int>       x = {2, 4, 6};
    ASSERT_TRUE(GetParam()(x.begin(), x.end(), [] (int v) -> bool {return (v % 2) == 0;}));}

TEST_P(AllOf_List_Fixture, class_example_2) {
    const list<int>       x = {2, 3, 6};
    ASSERT_FALSE(GetParam()(x.begin(), x.end(), [] (int v) -> bool {return (v % 2) == 0;}));}
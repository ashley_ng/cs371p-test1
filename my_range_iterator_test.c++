#include <algorithm> //equal
#include <list> //list

#include "gtest/gtest.h"
#include "my_range_iterator.h"

using namespace std;

/*
	Test from class
*/
TEST(Range_Iterator_Fixture, class_test_1) {
    Range_Iterator<int> b = 2;
    Range_Iterator<int> e = 2;
    ASSERT_EQ(b, e);}

TEST(Range_Iterator_Fixture, class_test_2) {
    Range_Iterator<int> b = 2;
    Range_Iterator<int> e = 3;
    ASSERT_NE(b, e);
    ASSERT_EQ(2, *b);
    ++b;
    ASSERT_EQ(b, e);}

TEST(Range_Iterator_Fixture, class_test_3) {
    Range_Iterator<int> b = 2;
    Range_Iterator<int> e = 4;
    ASSERT_NE(b, e);
    ASSERT_EQ(2, *b);
    ++b;
    ASSERT_NE(b, e);
    ASSERT_EQ(3, *b);
    b++;
    ASSERT_EQ(b, e);}

TEST(Range_Iterator_Fixture, class_test_4) {
    list<int> x = {2, 3, 4};
    ASSERT_TRUE(equal(Range_Iterator<int>(2), Range_Iterator<int>(5), x.begin()));}
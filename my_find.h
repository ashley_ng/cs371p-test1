template<typename InputIterator, typename T>
InputIterator my_find(InputIterator begin, InputIterator end, T value) {
	while (begin != end) {
		if (*begin == value)
			return begin; 
		++begin;
	}
	return begin;
}


template<typename ForwardIterator, typename Func>
bool my_all_of(ForwardIterator begin, ForwardIterator end, Func f) {
	while (begin != end) {
		if (!f(*begin))
			return false;
		++begin;
	}
	return true;
}
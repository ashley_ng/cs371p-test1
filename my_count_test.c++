#include <functional> // function
#include <iostream>   // count
#include <list>       // list

#include "gtest/gtest.h"
#include "my_count.h"

using namespace std;
using testing::TestWithParam;
using testing::Values;

using Count_List_Signature = function<int (list<int>::const_iterator, list<int>::const_iterator, int)>;
struct Count_List_Fixture : TestWithParam<Count_List_Signature>
	{};
INSTANTIATE_TEST_CASE_P(
	Count_List_Instantiation,
	Count_List_Fixture,
	Values(
		count<list<int>::const_iterator, int>,
		my_count<list<int>::const_iterator, int>
	)
);

TEST_P(Count_List_Fixture, test_1) {
	list<int> l{2, 2, 2};
	const int value = 2;
	ASSERT_EQ(GetParam()(l.begin(), l.end(), value), 3);
}

TEST_P(Count_List_Fixture, test_2) {
	list<int> l{1, 2, 3, 4, 5, 6, 7};
	const int value = 9;
	ASSERT_EQ(GetParam()(l.begin(), l.end(), value), 0);
} 

TEST_P(Count_List_Fixture, test_3) {
	list<int> l{6, 2, 1, 4, 4, 6, 4};
	const int value = 4;
	ASSERT_EQ(GetParam()(l.begin(), l.end(), value), 3);
} 
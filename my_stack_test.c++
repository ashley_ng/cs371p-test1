#include <algorithm> //equal
#include <list> //list
#include <stack> //stack

#include "gtest/gtest.h"
#include "my_stack.h"

using namespace std;

TEST(Stack_Fixture, test_1) {
	MyStack<int> stack;
	stack.push(2);
	stack.push(3);

	ASSERT_EQ(stack.peek(), 3);
	ASSERT_EQ(stack.size(), 2);
}

TEST(Stack_Fixture, test_2) {
	MyStack<int> stack;

	ASSERT_EQ(stack.size(), 0);
}

TEST(Stack_Fixture, test_3) {
	MyStack<int> stack;
	vector<int> v{4, 3, 2};

	stack.push(2);
	stack.push(3);
	stack.push(4);

	ASSERT_EQ(stack.size(), 3);
	ASSERT_EQ(stack.peek(), 4);
	ASSERT_TRUE(equal(v.begin(), v.end(), stack.begin()));
}

TEST(Stack_Fixture, test_4) {
	MyStack<int> stack;
	vector<int> v{4, 3, 2};

	stack.push(2);
	stack.push(3);
	stack.push(4);

	ASSERT_EQ(stack.size(), 3);
	ASSERT_EQ(stack.peek(), 4);

	MyStack<int>::MyIterator it = stack.end();
	--it;
	ASSERT_EQ(*it, 2);
	--it;
	ASSERT_EQ(*it, 3);
	--it;
	ASSERT_EQ(*it, 4);
}

TEST(Stack_Fixture, test_5) {
	MyStack<int> stack;
	vector<int> v{4, 3, 2};

	stack.push(2);
	stack.push(3);
	stack.push(4);

	MyStack<int>::MyIterator it = stack.begin();

	ASSERT_EQ(*it, 4);
	ASSERT_EQ(*(it++), 4);
}

// TEST(Stack_Fixture, test_6) {
// 	MyStack<int> stack;
// 	vector<int> v{4, 3, 2};

// 	stack.push(2);
// 	stack.push(3);
// 	stack.push(4);

// 	MyStack<int>::MyIterator it = stack.begin();
// 	MyStack<int>::MyIterator temp = stack.begin();

// 	ASSERT_EQ(*it, 4);
// 	temp = it++;
// 	ASSERT_EQ(*temp, 4);
// 	ASSERT_EQ(*it, 3);
// }
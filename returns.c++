#include <cassert> // assert
#include <iostream> // cout, endl;

// returns copy
int copy() {
	int i = 2;
	return i;
}

// returns pointer
int* pointer() {
	int i = 2;
	return &i; // returning local variables address, not safe
}

//return reference
int & reference() {
	int i = 2;
	return i; // returning reference to local variable, not safe
}

void return__copy() {
	cout << "return copy" << endl;
	int x = copy();
	assert(v == 2);
	cout << "return copy done" << endl;
}

void return_pointer() {
	cout << "return pointer" << endl;
	int x = *pointer();
	// assert(x == 2); // conditional jump

	int *y = pointer();
	// assert(*y == 2); // conditional jump

	int& z = *pointer();
	// assert(z == 2); // conditional jump
}